# Ethereum-based Wallet Key Generator

## About 

Simple wallet generator to generate throw-away-/one-time-wallets for ethereum based blockchains.
Just plain javascript and some trivial formattings to KISS.
Just go and use it: [wallet.mazl.io](http://wallet.mazl.io)

This website runs runs in your local and has no backend. So all the keys will never leave your machine, but of course do not use it for any serious use-cases.

## How to use it

1. Go to [wallet.mazl.io](http://wallet.mazl.io)
2. Just click `create wallet` to generate a new set of private and public keys.
3. Copy the private key.
4. Install and run **Metamask**
5. Click on accounts (that icon in the top-right corner)
6. **Import account** and paste your private key
7. Done. Use it like any other Metamask wallet.

If you want to delete after using it than just remove the account again.


## Contributing
If there is anything to contribute, feel free...

## Donate
If you like this project feel free to donate some ETH, MATICs or whatever you like ETH-based tokens to **0x327167dFd9aD360e2EB42929d9F196430CcBcdac**.

## Authors and acknowledgment
Just me and ChatGPT.